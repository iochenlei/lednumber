package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class EightConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowBottom();
        ledNumber.lightColumnLeftTopHalf();
        ledNumber.lightColumnLeftBottomHalf();
        ledNumber.lightColumnRightTopHalf();
        ledNumber.lightColumnRightBottomHalf();
        ledNumber.lightRowMiddle();
        return ledNumber;
    }
}
