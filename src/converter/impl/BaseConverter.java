package converter.impl;

import core.LEDNumber;

public abstract class BaseConverter {

    private int width;
    private int height;

    public LEDNumber buildLEDNumber(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("The size must be greater than 0");
        }
        width = (int)Math.pow(2, size) + 1;
        height = 4 * size + 1;
        return new LEDNumber(height, width);
    }
}
