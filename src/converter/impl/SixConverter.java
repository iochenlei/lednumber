package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class SixConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowBottom();
        ledNumber.lightRowMiddle();
        ledNumber.lightColumnLeftTopHalf();
        ledNumber.lightColumnLeftBottomHalf();
        ledNumber.lightColumnRightBottomHalf();
        return ledNumber;
    }
}
