package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class TwoConverter extends BaseConverter implements NumberConverter {
    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowBottom();
        ledNumber.lightRowMiddle();
        ledNumber.lightColumnRightTopHalf();
        ledNumber.lightColumnLeftBottomHalf();
        return ledNumber;
    }
}
