package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class FiveConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightColumnLeftTopHalf();
        ledNumber.lightRowMiddle();
        ledNumber.lightRowBottom();
        ledNumber.lightColumnRightBottomHalf();
        return ledNumber;
    }
}
