package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class SevenConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightColumnRight();
        return ledNumber;
    }
}
