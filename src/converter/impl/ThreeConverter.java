package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class ThreeConverter extends BaseConverter implements NumberConverter {
    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowMiddle();
        ledNumber.lightRowBottom();
        ledNumber.lightColumnRightTopHalf();
        ledNumber.lightColumnRightBottomHalf();
        return ledNumber;
    }
}
