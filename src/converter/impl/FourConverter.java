package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class FourConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightColumnLeftTopHalf();
        ledNumber.lightColumnRightTopHalf();
        ledNumber.lightColumnRightBottomHalf();
        ledNumber.lightRowMiddle();
        return ledNumber;
    }
}
