package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class OneConverter extends BaseConverter implements NumberConverter {
    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightColumnRight();
        return ledNumber;
    }
}
