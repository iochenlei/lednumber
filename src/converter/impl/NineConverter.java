package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class NineConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowMiddle();
        ledNumber.lightColumnLeftTopHalf();
        ledNumber.lightColumnRightTopHalf();
        ledNumber.lightColumnRightBottomHalf();
        ledNumber.lightRowBottom();
        return ledNumber;
    }
}
