package converter.impl;

import converter.NumberConverter;
import core.LEDNumber;

public class ZeroConverter extends BaseConverter implements NumberConverter {


    @Override
    public LEDNumber toLEDNumber(int size) {
        LEDNumber ledNumber = buildLEDNumber(size);
        ledNumber.lightRowTop();
        ledNumber.lightRowBottom();
        ledNumber.lightColumnLeft();
        ledNumber.lightColumnRight();
        return ledNumber;
    }
}
