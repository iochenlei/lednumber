package converter;

import core.LEDNumber;

public interface NumberConverter {
    LEDNumber toLEDNumber(int size);
}
