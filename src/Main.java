import converter.NumberConverter;
import converter.impl.*;
import core.LEDNumber;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    private final static Map<String, NumberConverter> converterMap = new HashMap<>();

    static {
        converterMap.put("0", new ZeroConverter());
        converterMap.put("1", new OneConverter());
        converterMap.put("2", new TwoConverter());
        converterMap.put("3", new ThreeConverter());
        converterMap.put("4", new FourConverter());
        converterMap.put("5", new FiveConverter());
        converterMap.put("6", new SixConverter());
        converterMap.put("7", new SevenConverter());
        converterMap.put("8", new EightConverter());
        converterMap.put("9", new NineConverter());
    }

    public static void main(String argv[]) {
        String input = readInput(argv);
        int size = readSize(argv);

        List<LEDNumber> ledNumberList = Arrays.stream(input.split(""))
                .filter(converterMap::containsKey)
                .map(ch -> converterMap.get(ch).toLEDNumber(size))
                .collect(Collectors.toList());
        showNumbers(ledNumberList);
    }

    private static int readSize(String[] argv) {
        for (int i = 0; i < argv.length; i++) {
            if (argv[i].matches("--size=\\d+")) {
                return Integer.parseInt(argv[i].split("=")[1]);
            }
        }
        return 1;
    }

    private static String readInput(String[] argv) {
        for (int i = 0; i < argv.length; i++) {
            if (argv[i].matches("--number=\\d+")) {
                return argv[i].split("=")[1].trim();
            }

        }
        throw new IllegalArgumentException("You must be specified the number");
    }

    private static void showOneNumber(LEDNumber ledNumber) {
        for(int i = 0; i < ledNumber.getHeight(); i++) {
            for (int j = 0; j < ledNumber.getWidth(); j++) {
                System.out.print(ledNumber.getChar(i, j));
            }
            System.out.println();
        }
    }

    private static void showNumbers(List<LEDNumber> ledNumberList) {
        if (ledNumberList.size() == 0) {
            throw new IllegalArgumentException("Must be have a number at least");
        }
        int totalWidth = ledNumberList.stream().mapToInt(number -> number.getWidth() + 1).sum();
        LEDNumber ledNumber = new LEDNumber(ledNumberList.get(0).getHeight(), totalWidth);
        int columnIndex = 0;
        for (int i = 0; i < ledNumberList.size(); i++) {
            LEDNumber current = ledNumberList.get(i);
            for (int y = 0; y < current.getHeight(); y++) {
                for (int x = 0; x < current.getWidth(); x++) {
                    ledNumber.putChar(y, columnIndex + x, current.getChar(y, x));
                }
            }
            columnIndex += current.getWidth() + 1;
        }
        showOneNumber(ledNumber);
    }
}
