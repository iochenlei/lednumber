package core;

public class LEDNumber {
    private char[][] data;
    private int height;
    private int width;

    public LEDNumber(int height, int width) {
        this.width = width;
        this.height = height;
        data = new char[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                data[i][j] = ' ';
            }
        }
    }

    public void putChar(int y, int x, char ch) {
        data[y][x] = ch;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public char getChar(int row, int column) {
        return data[row][column];
    }

    public void lightColumn(int column) {
        for (int y = 1; y < height - 1; y++) {
            putChar(y, column, '|');
        }
    }

    public void lightRow(int row) {
        for (int x = 1; x < width - 1; x++) {
            putChar(row, x, '-');
        }
    }

    public void lightRowTop() {
        lightRow(0);
    }

    public void lightRowBottom() {
        lightRow(height - 1);
    }

    public void lightColumnLeft() {
        lightColumn(0);
    }

    public void lightColumnRight() {
        lightColumn(width - 1);
    }

    public void lightRowMiddle() {
        lightRow(height / 2);
    }

    public void lightColumnLeftTopHalf() {
        for (int y = 1; y < height / 2; y++) {
            putChar(y, 0, '|');
        }
    }

    public void lightColumnRightTopHalf() {
        for (int y = 1; y < height / 2; y++) {
            putChar(y, width - 1, '|');
        }
    }

    public void lightColumnRightBottomHalf() {
        for (int y = height / 2 + 1; y < height - 1; y++) {
            putChar(y, width - 1, '|');
        }
    }

    public void lightColumnLeftBottomHalf() {
        for (int y = height / 2 + 1; y < height - 1; y++) {
            putChar(y, 0, '|');
        }
    }
}
